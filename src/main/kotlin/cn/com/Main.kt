

import cn.com.fhc.ros.Ros
import cn.com.fhc.ros.ConnectCallback
import cn.com.fhc.ros.Topic
import cn.com.fhc.ros.TopicCallback
import cn.com.fhc.ros.messages.Message
import jakarta.websocket.Session

fun main(args: Array<String>) {

    var lock = Object()
    val ros = Ros("192.168.3.211")

    ros.connect(object : ConnectCallback {
        override fun handleConnection(session: Session?) {
            println("handleConnection")

            val echoBack = Topic(ros, "/state", "std_msgs/Int8")
            echoBack.subscribe(object : TopicCallback {
                override fun handleMessage(message: Message) {
                    println( "From ROS: $message")

                    val echo = Topic(ros, "/action", "std_msgs/Int8")
                    val toSend = Message("{\"data\": 99}")
                    echo.publish(toSend)
                }

            })

//            ros.disconnect()

        }

        override fun handleDisconnection(session: Session?) {
            println("handleDisconnection")
        }

        override fun handleError(session: Session?, t: Throwable?) {
            println("handleError")
        }

    })

    while(true){

        synchronized(lock){

            System.out.println( "2.无限期等待中..." );

            lock.wait(); //等待，直到其它线程调用 lock.notify()

        }  }
}