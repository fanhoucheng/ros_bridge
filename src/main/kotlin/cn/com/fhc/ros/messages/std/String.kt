package cn.com.fhc.ros.messages.std

import cn.com.fhc.ros.messages.Message
import javax.json.Json
import javax.json.JsonObject


/**
 * The std_msgs/String message.
 *
 * @author fanhoucheng@sina.com
 * @version April fanhoucheng@sina.com
 */
class String: Message {
    private var data: kotlin.String = ""

    init{
    }

    constructor() : super(Json.createObjectBuilder().add(FIELD_DATA, "").build(), TYPE){
        this.data =  ""
    }
    constructor(data: kotlin.String) : super(Json.createObjectBuilder().add(FIELD_DATA, data).build(), TYPE){

        this.data = data
    }
    /**
     * Create a clone of this String.
     */
    override fun clone(): String {
        return String(data)
    }

    companion object {
        /**
         * The name of the data field for the message.
         */
        const val FIELD_DATA = "data"

        /**
         * The message type.
         */
        const val TYPE = "std_msgs/String"

        /**
         * Create a new String based on the given JSON string. Any missing values
         * will be set to their defaults.
         *
         * @param jsonString
         * The JSON string to parse.
         * @return A String message based on the given JSON string.
         */
        fun fromJsonString(jsonString: kotlin.String): String {
            // convert to a message
            return fromMessage(Message(jsonString))
        }

        /**
         * Create a new String based on the given Message. Any missing values will
         * be set to their defaults.
         *
         * @param m
         * The Message to parse.
         * @return A String message based on the given Message.
         */
        fun fromMessage(m: Message): String {
            // get it from the JSON object
            return fromJsonObject(m.toJsonObject())
        }

        /**
         * Create a new String based on the given JSON object. Any missing values
         * will be set to their defaults.
         *
         * @param jsonObject
         * The JSON object to parse.
         * @return A String message based on the given JSON object.
         */
        fun fromJsonObject(jsonObject: JsonObject): String {
            // check the fields
            val data = if (jsonObject.containsKey(FIELD_DATA)) jsonObject
                .getString(FIELD_DATA) else ""
            return String(data)
        }
    }
    /**
     * Create a new String with the given data value.
     *
     * @param data
     * The data value of the String.
     */
}
