package cn.com.fhc.ros.messages

import cn.com.fhc.ros.EMPTY_JSON
import cn.com.fhc.ros.JsonWrapper
import java.io.StringReader
import javax.json.Json
import javax.json.JsonObject

 open class Message: JsonWrapper {

    private var messageType: String? = null
        get() = field                     // getter
        set(value) { field = value }      // sette

    init{

    }

    constructor() : super(){
        this.messageType = ""
    }

    constructor(jsonString: String, messageType: String?= "") : super(jsonString) {
        this.messageType = messageType
    }

    constructor(jsonObject: JsonObject, messageType: String?= ""): super(jsonObject) {
        this.messageType = messageType
    }


    /**
     * Create a clone of this Message.
     */
    override fun clone(): JsonWrapper {
        return Message(toJsonObject(), this.messageType)
    }
}