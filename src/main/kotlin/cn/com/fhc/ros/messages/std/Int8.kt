package cn.com.fhc.ros.messages.std

import cn.com.fhc.ros.messages.Message
import javax.json.Json
import javax.json.JsonObject


/**
 * The std_msgs/Int8 message.
 *
 * @author fanhoucheng@sina.com
 * @version April fanhoucheng@sina.com
 */
class Int8: Message {
    companion object {
        const val FIELD_DATA = "data"
        const val TYPE = "std_msgs/Int8"

        /**
         * Create a new Int8 based on the given JSON string. Any missing values will
         * be set to their defaults.
         *
         * @param jsonString
         * The JSON string to parse.
         * @return A Int8 message based on the given JSON string.
         */
        fun fromJsonString(jsonString: kotlin.String): Int8 {
            // convert to a message
            return fromMessage(Message(jsonString))
        }

        /**
         * Create a new Int8 based on the given Message. Any missing values will be
         * set to their defaults.
         *
         * @param m
         * The Message to parse.
         * @return A Int8 message based on the given Message.
         */
        fun fromMessage(m: Message): Int8 {
            // get it from the JSON object
            return fromJsonObject(m.toJsonObject())
        }

        /**
         * Create a new Int8 based on the given JSON object. Any missing values will
         * be set to their defaults.
         *
         * @param jsonObject
         * The JSON object to parse.
         * @return A Int8 message based on the given JSON object.
         */
        fun fromJsonObject(jsonObject: JsonObject): Int8 {
            // check the fields
            val data =
                if (jsonObject.containsKey(FIELD_DATA)) jsonObject
                    .getInt(FIELD_DATA).toByte() else 0
            return Int8(data)
        }
    }

    private var data: Byte = 0

    init{
    }

    constructor() : super(Json.createObjectBuilder().add(FIELD_DATA, (0.toByte()).toInt()).build(), TYPE){
        this.data = 0.toByte()
    }
    constructor(data: Byte) : super(Json.createObjectBuilder().add(FIELD_DATA, data.toInt()).build(), TYPE){
        this.data = data
    }

    /**
     * Get the data value of this byte.
     *
     * @return The data value of this byte.
     */
    fun getData(): Byte {
        return data
    }

    /**
     * Create a clone of this Int8.
     */
    override fun clone(): Int8 {
        return Int8(data)
    }


}
