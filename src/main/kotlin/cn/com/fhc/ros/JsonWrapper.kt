package cn.com.fhc.ros

import java.io.StringReader
import javax.json.Json
import javax.json.JsonObject


abstract class JsonWrapper{
    var jsonObject :JsonObject
    var jsonString: String
    init {
    }
    constructor(): this(EMPTY_JSON) {

    }

    constructor(jsonString: String) {
        //次构造函数初始化代码块
        this.jsonString = jsonString
        this.jsonObject = Json.createReader(StringReader(jsonString)).readObject()
    }

    constructor(jsonObject: JsonObject) {
        this.jsonObject = jsonObject
        this.jsonString = this.jsonObject.toString()
    }


    /**
     * Get the JSON object.
     *
     * @return The JSON object.
     */
    open fun toJsonObject(): JsonObject {
        return jsonObject
    }

    /**
     * Get the String representation of this JSON object in JSON format.
     *
     * @return The String representation of this JSON object in JSON format.
     */
    override fun toString(): String {
        return jsonString
    }

    /**
     * Create a clone of this JSON object.
     */
    abstract fun clone(): JsonWrapper

    /**
     * Return the hash code of this JSON object, which is the hash code of the
     * JSON string.
     *
     * @return The hash code of the message.
     */
    override fun hashCode(): Int {
        return jsonString.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        return (other === this || other is JsonWrapper && jsonString == other.toString())
    }
}