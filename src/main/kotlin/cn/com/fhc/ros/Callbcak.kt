package cn.com.fhc.ros

import cn.com.fhc.ros.messages.Message
import cn.com.fhc.ros.services.ServiceRequest
import cn.com.fhc.ros.services.ServiceResponse
import jakarta.websocket.Session


interface ServiceRequestCallback {
    /**
     * This function is called when an incoming service request is received for
     * a given service request. No ROS type checking is done on the internal
     * data.
     *
     * @param request
     *            The service request that was received.
     */
    fun handleServiceCall(request: ServiceRequest)
}

interface ServiceResponseCallback {
    /**
     * This function is called when an incoming service response is received for
     * a given service request. No ROS type checking is done on the internal
     * data. A flag indicating if the call was successful is given.
     *
     * @param response
     * The service response that was received.
     */
    fun handleServiceResponse(response: ServiceResponse)
}
interface TopicCallback {
    /**
     * This function is called when an incoming message is received for a given
     * topic. No ROS type checking is done on the internal message data.
     *
     * @param message
     * The message that was received.
     */
    fun handleMessage(message: Message)
}


interface ConnectCallback {
    /**
     * Handle the connection event. This occurs during a successful connection
     * to rosbridge.
     *
     * @param session
     * The session associated with the connection.
     */
    fun handleConnection(session: Session?)

    /**
     * Handle the disconnection event. This occurs during a successful
     * disconnection from rosbridge.
     *
     * @param session
     * The session associated with the disconnection.
     */
    fun handleDisconnection(session: Session?)

    /**
     * Handle the error event.
     *
     * @param session
     * The session associated with the error.
     * @param t
     * The error.
     */
    fun handleError(session: Session?, t: Throwable?)
}