package cn.com.fhc.ros.services

import cn.com.fhc.ros.EMPTY_JSON
import cn.com.fhc.ros.JsonWrapper
import java.io.StringReader
import javax.json.Json
import javax.json.JsonObject

class ServiceResponse() : JsonWrapper() {
    private lateinit var serviceResponseType: String
    var result = false

    constructor(
        jsonString: String? = EMPTY_JSON,
        serviceResponseType: String,
        result: Boolean = true
    ) : this() {
        this.jsonObject = Json.createReader(StringReader(jsonString)).readObject()
        this.serviceResponseType = serviceResponseType
        this.result = result
    }

    constructor(jsonObject: JsonObject, result: Boolean) : this() {
        this.jsonObject = jsonObject
        this.result = result
    }

    override fun clone(): JsonWrapper {
        return ServiceRequest(toJsonObject(), this.serviceResponseType)
    }

}
