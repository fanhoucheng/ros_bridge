package cn.com.fhc.ros.services

import cn.com.fhc.ros.JsonWrapper
import javax.json.JsonObject

class ServiceRequest() : JsonWrapper() {
    private lateinit var serviceRequestType: String
    var id: String? = null
    init {
//        jsonObject = Json.createReader(StringReader(EMPTY_JSON)).readObject()
    }
    constructor(jsonObject: JsonObject): this() {
        // set the type
        this.jsonObject = jsonObject
        this.serviceRequestType = serviceRequestType
        this.id = ""
    }

    constructor(jsonObject: JsonObject, serviceRequestType: String): this() {
        this.jsonObject = jsonObject
        this.serviceRequestType = serviceRequestType
        this.id = ""
    }
    /**
     * Get the type of the service request if one was set.
     *
     * @return The type of the service request.
     */
    fun getServiceRequestType(): String? {
        return this.serviceRequestType
    }

    /**
     * Set the type of the service request.
     *
     * @param serviceRequestType
     * The type of the service request (e.g., "std_srvs/Empty").
     */
    fun setServiceRequestType(serviceRequestType: String) {
        this.serviceRequestType = serviceRequestType
    }


    override fun clone(): JsonWrapper {
        return ServiceRequest(toJsonObject(), this.serviceRequestType)
    }
}