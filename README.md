# ros_bridge

#### 介绍
一个简单的ROSBridge通讯的客户端


#### 软件架构
使用WebSocket和ROSBridge通讯，使用Kotlin开发，同时支持Android和纯Java。


- 几行代码搞定Publish 和 Subscribe，极易使用
- 强烈不推荐RosAndroid，复杂，庞大，不易使用



#### 目前功能

1.  目前支持 Publish 和 Subscribe
2.  目前支持 消息类型:Int8&String，可自由扩张其他类型


#### 使用说明

1.  编译jar  ./gradlew build  or ./gradlew jar  or ./gradlew fatjar  
2.  编译jar直接使用或复制源码

#### 其他说明

1.  启动rosbridge_server：roslaunch rosbridge_server rosbridge_websocket.launch
2.  Topic(/state)模拟发送消息：rostopic pub -r 10 /state std_msgs/Int8 '1'
3.  观察jar库Publish到Topic(/action)：rostopic echo /action
以上均在Ubuntu或树莓派上cmd执行
 
