
import org.gradle.jvm.tasks.Jar
plugins {
    kotlin("jvm") version "1.5.31"
    java
}

group = "cn.com.fhc"
version = "1.0.1"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    //此方式编译不报错，运行时会报找不到class
//    implementation("org.glassfish.tyrus.bundles:tyrus-standalone-client-jdk:2.0.2")
//    implementation("javax.json:javax.json-api:1.1.4")
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
}

sourceSets {

}
java {
    withSourcesJar()
//    withJavadocJar()
}

// Terminal  ./gradlew build  ./gradlew jar    ./gradlew fatjar
tasks.jar {
//    manifest.attributes["Main-Class"] = "com.example.MyMainClass"
    manifest.attributes["Class-Path"] = configurations
        .runtimeClasspath
        .get()
        .joinToString(separator = " ") { file ->
            "libs/${file.name}"
        }
}
tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        jvmTarget = "11"
    }
}

//tasks.getByName<Test>("test") {
//    useJUnitPlatform()
//}

//val mainClass = "cn.com.fhc.Main" // replace it!
tasks {
    register("fatJar", Jar::class.java) {
        archiveClassifier.set("all")
        duplicatesStrategy = DuplicatesStrategy.EXCLUDE
        manifest {
//            attributes("Main-Class" to mainClass)
        }
        from(configurations.runtimeClasspath.get()
            .onEach { println("add from dependencies: ${it.name}") }
            .map { if (it.isDirectory) it else zipTree(it) })
        val sourcesMain = sourceSets.main.get()
        sourcesMain.allSource.forEach { println("add from sources:${it.absolutePath} ") }
        from(sourcesMain.output)
    }
}

